"use strict";
const {
  task
} = require('gulp');
var nodemon = require('gulp-nodemon');
task('default', function (done) {
  let env = process.env.NODE_ENV;
  if (!env) {
    env = 'development';
  }
  if (env === 'development') {
    return nodemon({
      script: './bin/www',
      ext: 'js json',
      env: {
        "NODE_ENV": "development"
      },
      ignore: [
        'node_modules/'
      ],
      watch: [
        './bin/www',
        './api-services/', 
        './Server.js', 
        './gulpfile.js',
        './socket.js'
      ],
      stdout: true,
      readable: true
    }).on('start', function () {
      done();
    });
  }
});
