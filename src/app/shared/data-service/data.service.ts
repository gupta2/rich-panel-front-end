import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { Options } from 'selenium-webdriver/ie';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  apiurl: string = environment.apiurl;

  constructor(private http: HttpClient, private cookie: CookieService) { }

  notify() {
    const token = localStorage.getItem('token');
  }

  post(url, body) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    headers.append("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
    return this.http.post(this.apiurl + url, body);
  }

  get(url) {
    return this.http.get(this.apiurl + url);
  }

  postAuth(url, body, user = '') {
    const token = localStorage.getItem('token');
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': token
    });
    headers.append('x-access-token', token);
    let options = {
      headers: headers
    }
    return this.http.post(this.apiurl + url, body, options);
  }

  getAuth(url, user = '') {
    const token = localStorage.getItem('token');
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': token
    });
    let options = {
      headers: headers
    }
    return this.http.get(this.apiurl + url, options);
  }
}
