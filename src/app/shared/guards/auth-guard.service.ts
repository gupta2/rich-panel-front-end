import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../data-service/data.service';
import { Observable } from 'rxjs';

@Injectable()
export class IsLogin implements CanActivate {
  constructor(private _Router: Router, private _CookieService: CookieService, private _ActivatedRoute: ActivatedRoute, private _api: DataService) { }

  canActivate() {
    let token = localStorage.getItem('token');
    if (token) {
      return true;
    }
    else {
      return false;
    }
  } 
}

