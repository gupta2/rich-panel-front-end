import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/shared/data-service/data.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, NavigationEnd } from '@angular/router';
import { validateConfig } from '@angular/router/src/config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hideLogin: boolean = false;
  hideRegister: boolean = true;
  hideForgot: boolean = true;
  submitted: boolean = false;

  mobilePattern = "^[6-9][0-9]{9}";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
  onlyStrings = "^[a-zA-Z]+$";

  registerForm: FormGroup;
  loginForm: FormGroup;
  forgotForm: FormGroup;

  error = 'false';
  user = 'Parnter';
  userVal = true;
  regMess: string = "";
  validUser: boolean = true
  forgotPasswordStatus: Boolean = false

  constructor(private formBuilder: FormBuilder, private _RESTServices: DataService, private _CookieService: CookieService, private _router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15), Validators.pattern(this.onlyStrings)]],
      email_id: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      phone_number: ['', [Validators.required, Validators.pattern(this.mobilePattern)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    })
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      password: ['', Validators.required]
    });
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]]
    });
  }


  onSubmitLogin(): void {
    this.error = null;
    this._RESTServices.post('auth/login', this.loginForm.value).subscribe((response) => {
      if (response['status']) {
        localStorage.setItem('token', response['token'])
        this._router.navigate(['/home'])
      } else {
        this.validUser = false;
        this.error = response['message'];
      }
    })
  }

  onSubmitRegister() {
    console.log("this.register.value", this.registerForm.value);
    this._RESTServices.post('auth/register', this.registerForm.value).subscribe((response) => {
      console.log("response", response);
      if (response['status']) {
        this.regMess = response['message'];
        setTimeout(() => {
          this.hideRegister = true;
          this.hideForgot = true;
          this.hideLogin = false;
          this._router.navigate(['/login'])
        }, 3000);

      } else {
        this.regMess = response['message'];
      }
      // console.log("response", response);
    });
  }

  get lf() {
    return this.loginForm.controls;
  }
  get rf() {
    return this.registerForm.controls;
  }
  get fp() {
    return this.forgotForm.controls;
  }
  onClickRegister() {
    this.hideRegister = false;
    this.hideLogin = true;
    this.hideForgot = true;
  }
  OnClickSignIn() {
    this.hideLogin = false;
    this.hideRegister = true;
    this.hideForgot = true;
  }

  onClickforgotPassword() {
    this.hideForgot = false;
    this.hideRegister = true;
    this.hideLogin = true;
  }

  onForgotSubmit() {
    this._RESTServices.post("users/forgot/password", this.forgotForm.value).subscribe(res => {
      if (res['status_code'] == 200) {
        this.forgotPasswordStatus = true
      }
    }, err => {

    })
  }

}
