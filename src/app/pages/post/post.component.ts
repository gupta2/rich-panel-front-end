import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data-service/data.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  tweets: any;
  selectedId: any;
  threads: any;
  loggedUserId: any;

  constructor(private _dataService: DataService) { }

  ngOnInit() {
    this.loadTweets();
  }

  loadTweets() {
    this._dataService.postAuth('user-tweets', {}).subscribe(
      res => {
        // handle response case
        this.selectedId = res[0].id;
        this.tweets = res;
        this.loadThreads(this.tweets[0].tweet_id);
      },
      err => {
        // handle error case
        this.selectedId = 1;
        this.loggedUserId = 1;
        this.tweets = [
          {
            id: 1,
            user_screen_name: 'Naresh',
            profile_image_url: 'https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg',
            text: 'This is the tweet'
          },
          {
            id: 2,
            user_screen_name: 'Gupta',
            profile_image_url: 'https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg',
            text: 'Tweets are continues.....'
          }
        ];
        this.threads = this.threads = [
          {
            name: 'Naresh',
            userId: 1,
            profile_image_url: 'https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg',
            comments: 'This is the tweet',
            created_at: '10 Nov'
          },
          {
            name: 'Gupta',
            userId: 2,
            profile_image_url: 'https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg',
            comments: 'This is the tweet',
            created_at: '10 Nov'
          }
        ];
      }
    );
  }

  loadThreads(tweetId) {
    this._dataService.postAuth('tweet-threads', {}).subscribe(
      res => {
        this.threads = res;
        // handle response case
      },
      err => {
        // handle error case
        this.threads = [
          {
            name: 'Naresh',
            userId: 1,
            profile_image_url: 'https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg',
            comments: 'This is the tweet',
            created_at: '10 Nov'
          },
          {
            name: 'Gupta',
            userId: 2,
            profile_image_url: 'https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg',
            comments: 'This is the tweet',
            created_at: '10 Nov'
          }
        ];
      }
    );
  }

}
