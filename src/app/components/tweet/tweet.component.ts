import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss'],
})
export class TweetComponent implements OnInit {

  @Input() name: string;
  @Input() url: string;
  @Input() text: string;
  @Input() selected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
