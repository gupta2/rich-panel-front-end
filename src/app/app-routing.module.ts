import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { IsLogin } from './shared/guards/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { PostComponent } from './pages/post/post.component';
import { ThreadComponent } from '../app/components/thread/thread.component'

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'post', component: PostComponent },
  { path: 'thread', component: ThreadComponent },
  // { path: 'advertiser', loadChildren: "./advertiser/advertiser.module#AdvertiserModule", canActivate: [IsLogin] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
