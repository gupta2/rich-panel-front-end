import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/data-service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private dataService: DataService,private _router: Router) { }

  ngOnInit() {
  }

  loginTwitter() {
    console.log("twitter login")
    this.dataService.postAuth('twitter-login', {}).subscribe(
      res => {

      },
      err => {

      });
  }

}
