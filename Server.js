'use strict';
const express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  config = require('config'),
  favicon = require('serve-favicon'),
  compression = require('compression'),
  helmet = require('helmet'),
  cors = require('cors'),
  cookieParser = require('cookie-parser'),
  timeout = require('connect-timeout'),
  app = express();

/**
 * Native Db Connection Goes Here
 */


/**
 * Server Setup
 */
var processexit = false;
config.home_directory = __dirname;

const cacheoptions = {
  dotfiles: 'ignore',
  etag: false,
  extensions: ['js', 'css', 'jpg', 'png', 'jpeg', 'html'],
  index: false,
  // maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now())
  }
}

if (process.env.NODE_ENV !== 'development') {
  console.log('adding compression');
  app.use(compression());
}
app.use(timeout('1200s'))
app.use(helmet());
app.use(haltOnTimedout);
app.use(cookieParser())



// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({
  type: 'application/vnd.api+json'
})); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({
  extended: true
})); // parse application/x-www-form-urlencoded

app.engine('html', require('ejs').renderFile);
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
if (process.env.NODE_ENV !== 'development') {
  console.log('adding chache options');
  app.use(express.static(__dirname + '/dist', cacheoptions)); // set the static files location /public/img will be /img for users
} else {
  app.use(express.static(__dirname + '/dist')); // set the static files location /src/assets will be /img for users
}
// if (process.env.NODE_ENV !== 'development') {
//   app.use(favicon(__dirname + '/dist/favicon.ico'));
// } else {
//   app.use(favicon(__dirname + '/src/favicon.ico'));
// }
app.use(favicon(__dirname + '/src/favicon.ico'));

function logErrors(err, req, res, next) {
  console.log('in logErrors');
  console.error(err.stack)
  next(err)
}

function clientErrorHandler(err, req, res, next) {
  console.log('in clientErrorHandler');
  if (req.xhr) {
    res.status(500).send({
      error: 'Something failed!'
    })
  } else {
    next(err)
  }
}

app.use(logErrors);
app.use(clientErrorHandler);

app.use(cors(), function (req, res, next) {
  // res.header("Access-Control-Allow-Origin", config.cros);
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});
require('./api-services/routes/Routes')(app);

console.log("Server is running on", app.get('env'));
app.get('*', function (req, res, next) {
  if (app.get('env') != 'development') {
    res.sendFile(__dirname + '/dist/index.html');
  } else {
    res.sendFile(__dirname + '/src/index.html');
  }
});


app.internalError = function (err, code, res) {
  const error = {};
  error.message = err;
  error.status = 'error';
  res.statusCode = code;
  return res.send(error);
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: err
    });
  });
} else {
  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: {}
    });
  });
}

app.use(function (err, req, res, next) {
  res.end(err.message); // this catches the error!!
});

function haltOnTimedout(req, res, next) {
  if (!req.timedout) next()
}

if (app.get('env') === 'production') {
  const port = normalizePort(config.port);
  app.set('port', port);
  app.listen(port, config.host);
  console.log('Server is running on ', port)
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}


process.on('uncaughtException', function (uncaught_exception) {
  console.error((new Date).toUTCString() + ' uncaughtException:', uncaught_exception.message)
  console.error(uncaught_exception);
});

process.on('unhandledRejection', function (unhandled_error) {
  console.error((new Date).toUTCString() + ' unhandledRejection:', unhandled_error.message)
  console.error(unhandled_error);
});

process.on('SIGINT', function () {
  console.log(' on exit called by node');
  process.exit(1);
  processexit = true;
});

process.on('uncaughtException', function (e) {
  console.error('Uncaught Exception...');
  console.error(e.stack);
});


/* Routes */

exports = module.exports = app; // expose app
